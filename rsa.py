import prime as util 


def getrandomkey(bits):
    q = util.large(bits)
    p = util.large(bits)
    n = p*q
    phi_n = util.phin(p,q)
    if q < p:  
          exponent = util.exponent(p,phi_n)
    else: 
        exponent = util.exponent(q,phi_n)
    q = 0
    p = 0 
    k = util.modinv(exponent,phi_n)
    return [(exponent,n),(k,n)]



def encrypt(key, number):
    exponent = key[0][0]

    return pow(number,exponent,key[0][1])

def decrypt(key, number):
    k = key[1][0]
    exponent = key[0][0]

    return pow(number,k,key[0][1])













