import numpy as np
import math 
import random 
from Crypto.Util import number

def exponent(q, phi_n):
    for e in range(q, phi_n): 
        if(np.gcd(e,phi_n) != np.gcd(e,q)): i=+1
        elif(np.gcd(e,phi_n) ==1 and np.gcd(e,q) == 1): return e

def large(bits):
    return number.getPrime(bits)

def phin(p,q):
    return (p-1)*(q-1)

def n(p,q):
    return p*q

def iterative_egcd(a, b):
    x,y, u,v = 0,1, 1,0
    while a != 0:
        q,r = b//a,b%a; m,n = x-u*q,y-v*q 
        b,a, x,y, u,v = a,r, u,v, m,n
    return b, x, y


def modinv(a, m):
    g, x, y = iterative_egcd(a, m) 
    if g != 1:
        return None
    else:
        return x % m


def stringToNumber(string):
    number = []
    for i in string:
        number.append(str(ord(i)))
    return int("".join(number))






        