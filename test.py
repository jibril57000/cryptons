import rsa 
import base64
import os
from colorprint import * 
from Crypto.PublicKey import RSA




def encryptString(key, string): 
    byte = string.encode('utf-8')
    inte = int.from_bytes(byte,'little')
    e = str(rsa.encrypt(key, inte)).encode('utf-8')
    return base64.b64encode(e).decode('utf-8')

def decrypt(key, number):
    number= base64.b64decode(number)
    number = number.decode('utf-8')
    number =  int(number)
    decrypt = rsa.decrypt(key,number)
    recoveredbytes = decrypt.to_bytes((decrypt.bit_length() + 7) // 8, 'little')
    return recoveredbytes.decode('utf-8')

def menu():
    
    print(bcolors.OKCYAN + "------------------------" + bcolors.ENDC)
    print(bcolors.OKGREEN + "[1] Chiffrer")
    print(bcolors.OKGREEN + "[2] Déchiffrer")
    print(bcolors.OKGREEN + "[3] Chiffrer un fichier .txt")
    print(bcolors.OKGREEN + "[4] Déchiffrer un fichier .txt")

    print("[5] Option de chiffrement" + bcolors.ENDC)
    print(bcolors.OKCYAN + "------------------------" + bcolors.ENDC)

    print(f"┌──{bcolors.OKCYAN}Entrez votre choix{bcolors.ENDC}")
    choice = int(input("└───> "))

    if choice == 1:
        encryptscreen()
    elif choice == 2:
        decryptscreen()
    elif choice == 3:
        encryptfilescreen()
    elif choice == 4:
        decryptfile()
    elif choice == 5:
        keyscreen()

def encryptfilescreen():
    print(f"┌──{bcolors.OKCYAN}Enter le nom fichier{bcolors.ENDC}")
    choice = str(input("└───> "))
    file = open(choice, 'r+',encoding='utf-8')
    ligne = file.readlines()
    file.seek(0)
    file.truncate()
    lines = [encryptString(key,ligne.replace("\n", "")) + "\n" for ligne in ligne]
    file.writelines(lines)
    file.close()

def decryptfile():
    print(f"┌──{bcolors.OKCYAN}Enter le nom fichier{bcolors.ENDC}")
    choice = str(input("└───> "))
    file = open(choice, 'r+', encoding='utf-8')
    ligne = file.readlines()
    file.seek(0)
    file.truncate()
    lines = [decrypt(key,ligne)+ "\n" for ligne in ligne]
    file.writelines(lines)
    file.close()

def encryptscreen():
    print(f"┌──{bcolors.OKCYAN}Entrer votre chaine de caractère{bcolors.ENDC}")
    choice = str(input("└───> "))

    print(encryptString(key,choice))
    menu()

def decryptscreen():
    print(f"┌──{bcolors.OKCYAN}Entrer votre chaine de caractère chiffrée{bcolors.ENDC}")
    choice = str(input("└───> "))

    print(f"┌──{bcolors.OKCYAN}Chaine de caractère déchiffrée{bcolors.ENDC}")
    print(f"└──>{decrypt(key,choice)}")
    menu()


def keyscreen():
    print(bcolors.OKCYAN + "------------------------" + bcolors.ENDC)
    print(bcolors.OKGREEN + "[*] Taille de la clé: "+ str(keysize) + " bits")
    print(bcolors.OKGREEN + "[1] Générer un nouveau couple de clés")
    print(f"┌──{bcolors.OKCYAN}Entrer votre choix{bcolors.ENDC}")
    choice = int(input("└───> "))

    if choice == 1:
        keygenerate()


global keysize 
global key
keysize =256
key = rsa.getrandomkey(keysize)

def keygenerate():
    print(f"┌──{bcolors.OKCYAN}Taille n bits de la clé{bcolors.ENDC}")
    choice = int(input("└───> "))
    global key 
    key = rsa.getrandomkey(choice)
    global keysize 
    keysize = choice


    privateKey = RSA.construct((key[0][1], key[0][0], key[1][0]))
    pub = RSA.construct((key[0][1], key[0][0]))
    priv=privateKey.export_key()
    pubkey=pub.export_key()
    writeKeyFile(priv,pubkey)

    menu()

def writeKeyFile(priv, pub):
    file = open("private.pem", "wb")
    file.write(priv)
    file.close()

    file = open("pub.pem", "wb")
    file.write(pub)
    file.close()

def checkKey():
    if os.path.exists("private.pem") and os.path.exists("pub.pem"):
        _extracted_from_checkKey_3()
    else: 
        print("─> Génération d'un couple de clés 256 bits")
        privateKey = RSA.construct((key[0][1], key[0][0], key[1][0]))
        pub = RSA.construct((key[0][1], key[0][0]))
        priv=privateKey.export_key()
        pubkey=pub.export_key()
        writeKeyFile(priv,pubkey)

def _extracted_from_checkKey_3():
    print("───> Chargement des clés")
    global key
    file = open("private.pem", "rb")
    print("───> Clé privé chargée")

    keyp = RSA.import_key(file.read())

    file = open("pub.pem", "rb")
    print("───> Clé publique chargée")
    keypub = RSA.import_key(file.read())

    key = [(keypub.e, keypub.n), (keyp.d, keypub.n)]


checkKey()
menu()




